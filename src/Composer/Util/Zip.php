<?php











namespace Composer\Util;




class Zip
{








public static function getComposerJson($pathToZip)
{
if (!extension_loaded('zip')) {
throw new \RuntimeException('The Zip Util requires PHP\'s zip extension');
}

$zip = new \ZipArchive();
if ($zip->open($pathToZip) !== true) {
return null;
}

if (0 == $zip->numFiles) {
$zip->close();

return null;
}

$foundFileIndex = self::locateFile($zip, 'composer.json');
if (false === $foundFileIndex) {
$zip->close();

return null;
}

$content = null;
$configurationFileName = $zip->getNameIndex($foundFileIndex);
$stream = $zip->getStream($configurationFileName);

if (false !== $stream) {
$content = stream_get_contents($stream);
}

$zip->close();

return $content;
}









private static function locateFile(\ZipArchive $zip, $filename)
{
$indexOfShortestMatch = false;
$lengthOfShortestMatch = -1;

for ($i = 0; $i < $zip->numFiles; $i++) {
$stat = $zip->statIndex($i);
if (strcmp(basename($stat['name']), $filename) === 0) {
$directoryName = dirname($stat['name']);
if ($directoryName === '.') {

 
 return $i;
}

if (strpos($directoryName, '\\') !== false ||
strpos($directoryName, '/') !== false) {

 continue;
}

$length = strlen($stat['name']);
if ($indexOfShortestMatch === false || $length < $lengthOfShortestMatch) {

 $contents = $zip->getFromIndex($i);
if ($contents !== false) {
$indexOfShortestMatch = $i;
$lengthOfShortestMatch = $length;
}
}
}
}

return $indexOfShortestMatch;
}
}
